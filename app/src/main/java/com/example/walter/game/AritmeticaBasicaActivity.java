package com.example.walter.game;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class AritmeticaBasicaActivity extends AppCompatActivity {
    /*
    ➡ Deve ser apresentada uma equação aleatória do tipo:
    operando1 operador operando2. Sendo que operandos são
    números de 0 a 9 e operador apenas + e -.
    ➡O usuário deve fornecer a resposta em um campo de texto e o
    aplicativo deve informar, em um AlertDialog, se a resposta está
    correta ou não. Em caso de erro, informar ainda a resposta
    correta.
    ➡ Da mesma forma do item anterior, deve-se ter um total de 5
    perguntas, com equações diferentes e ao final informar a nota
    do usuário.
    * */
    static Random randomGen = new Random();
    static String operators = "+-";
    static String equation;
    static Integer count = 0,right=0;
    static Integer answerValue;
    static Set<String> eqStringSet = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aritmetica_basica);
        load();
        Button answerButton = findViewById(R.id.botao_confirmar_aritimetica);
        answerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    EditText editText = findViewById(R.id.edit_resposta_aritmetica);
                    Integer answer = Integer.parseInt(editText.getText().toString());
                    String msg = "";
                    AlertDialog.Builder builder = new AlertDialog.Builder(AritmeticaBasicaActivity.this);
                    builder.setTitle("Resultado");
                    if (answer == resolveEq(equation)) {
                        builder.setMessage("Resposta Correta!");
                        msg = "Acertou! \n";
                        right += 1;
                    } else {
                        builder.setMessage("Resposta Errada! A resposta correta era: " + answerValue);
                        msg = "Resposta Errada! A resposta correta era: " + answerValue + "\n";
                    }
                    count++;
                    if (count == 5) {
                        builder.setMessage(msg + "\nResultado final: " + right + " acertos. NOTA: " + showResult());
                    }
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (count < 5) {
                                load();
                            } else {
                                restartGame();
                            }
                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }catch (NumberFormatException e){
                    Toast.makeText(AritmeticaBasicaActivity.this, "Por favor insira um número válido!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public static char getRandomOperator(){ return operators.charAt(randomGen.nextInt(operators.length())); }

    public static int getRandomNumber(){
        return randomGen.nextInt(9);
    }

    public  static String createRandomEq(){
        String equation = "";
        int numOfOperators = 1;
        char operator = ' ';
        for (int i = 0; i < numOfOperators; i++) {
            equation += getRandomNumber();
            equation += getRandomOperator();
        }
        int number = getRandomNumber();
        if(equation.charAt(1) == '-') {
            while (number > Character.getNumericValue(equation.charAt(0))) {
                number = getRandomNumber();
            }
        }
        equation += number;
        return equation;
    }
    public static Integer resolveEq(String equation){
        int value1 = Integer.parseInt(String.valueOf(equation.charAt(0)));
        String op = String.valueOf(equation.charAt(1));
        int value2 = Integer.parseInt(String.valueOf(equation.charAt(2)));

        if(op.equals("+")){
            return value1+value2;
        }
        return value1-value2;
    }
    public void load(){
        //logica de criação de pergunta/resp
        equation = createRandomEq();
        //verifico se ja foi perguntada essa equação, para que todas sejam diferentes
        if(eqStringSet.contains(equation)){
            while (eqStringSet.contains(equation))
                equation = createRandomEq();
        }
        eqStringSet.add(equation);

        TextView textView = findViewById(R.id.label_equacao_aritmetica);
        textView.setText(equation);

        answerValue = resolveEq(equation);
    }
    public Integer showResult(){return (right * 100) / 5;}

    public void restartGame(){
        //metodo que limpa as variáveis e reinicia o jogo;
        count=0;
        right=0;
        eqStringSet.clear();
        EditText editText = findViewById(R.id.edit_resposta_aritmetica);
        editText.setText("");
        load();
    }
}
