package com.example.walter.game;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class ContagemActivity extends AppCompatActivity {
    /*
    ➡ Apresenta uma figura com uma quantidade de itens aleatória e
    3 opções de escolha (números), sendo que uma delas representa
    exatamente a quantidade de itens da figura. A criança deve
    contar os itens e escolher uma das opções de resposta.
    ➡ O aplicativo deve informar, em um AlertDialog, se a resposta
    está correta ou não. Em caso de erro, informar ainda a resposta
    correta.
    ➡ Deve ser apresentado um total de 5 perguntas consecutivas.
    Para cada pergunta use uma imagem diferente. O seu app deve
    conter um total de 10 imagens e sortear uma imagem para cada
    pergunta. Não deve haver repetição de perguntas.
    ➡ Ao final do jogo informe a porcentagem de acertos em forma
    de nota (1 a 100).
    * */
    Map<Integer,Integer> mapRespostas;
    List<Integer>listaPerguntas = new ArrayList<>();
    Random random = new Random();
    Integer resposta=0;
    Set<Integer> imgsCarregadas = new HashSet<>();
    Integer contador=0;
    Integer qtdAcertadas = 0;
    ImageView imagem;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contagem);
        try {
            mapRespostas = new HashMap<>();
            mapRespostas.put(R.drawable.one, 1);
            mapRespostas.put(R.drawable.two, 2);
            mapRespostas.put(R.drawable.three, 3);
            mapRespostas.put(R.drawable.four, 4);
            mapRespostas.put(R.drawable.five, 5);
            mapRespostas.put(R.drawable.six, 6);
            mapRespostas.put(R.drawable.seven, 7);
            mapRespostas.put(R.drawable.eight, 8);
            mapRespostas.put(R.drawable.nine, 9);
            mapRespostas.put(R.drawable.ten, 10);

            carregar();
            //Até aqui estou carregando a Imagem e os valores dos radioButtons;
            RadioGroup radioGroup = findViewById(R.id.radio_group_contagem);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton radioButton;
                    switch (checkedId) {
                        case R.id.radio_button1_contagem:
                            radioButton = findViewById(R.id.radio_button1_contagem);
                            verificarResposta(radioButton);
                            break;
                        case R.id.radio_button2_contagem:
                            radioButton = findViewById(R.id.radio_button2_contagem);
                            verificarResposta(radioButton);
                            break;
                        case R.id.radio_button3_contagem:
                            radioButton = findViewById(R.id.radio_button3_contagem);
                            verificarResposta(radioButton);
                            break;
                    }
                }
            });
        } catch (Exception e){
            Toast.makeText(this, "Erro:"+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    public void carregarImagem(ImageView imagem){
        int i = random.nextInt(10);
        if(imgsCarregadas.contains(i) || i==0) {
            while(imgsCarregadas.contains(i) ||      i==0)
                i=random.nextInt(10);
        }
        imgsCarregadas.add(i);
        imagem.setTag(i);
        imagem.setImageResource(getKey(i));
    }

    private Integer getKey(Integer value){
        for(Integer key : mapRespostas.keySet()){
            if(mapRespostas.get(key).equals(value)){
                return key;
            }
        }
        return null;
    }

    public void verificarResposta(RadioButton rb){
        contador+=1;
        AlertDialog.Builder builder = new AlertDialog.Builder(ContagemActivity.this);
        builder.setTitle("Resultado");
        String msg="";
        if(Integer.parseInt(rb.getText().toString()) == resposta){
            msg = "Resposta Correta !";
            qtdAcertadas+=1;
        }
        else{
            msg ="Resposta Errada ! A resposta era : "+resposta.toString();
        }
        if(contador == 5){
            msg+="\n Resultado Final : "+qtdAcertadas+" acertos, Nota: "+(qtdAcertadas * 100) / 5;
        }
        builder.setMessage(msg);
        builder.setPositiveButton("OK",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (contador<5){
                    carregar();
                }
                else{
                   contador=0;
                   imgsCarregadas.clear();
                   qtdAcertadas=0;
                   carregar();
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        rb.setChecked(false);
    }
    public void carregar(){
        imagem = findViewById(R.id.imagem_contagem);
        carregarImagem(imagem);

        resposta = Integer.parseInt(imagem.getTag().toString());

        radioButton1 = findViewById(R.id.radio_button1_contagem);
        radioButton2 = findViewById(R.id.radio_button2_contagem);
        radioButton3 = findViewById(R.id.radio_button3_contagem);

        //guardo os radioButtons em uma lista
        List<RadioButton> radioButtonList = new ArrayList<>();
        Collections.addAll(radioButtonList, radioButton1, radioButton2, radioButton3);
        int radioButtonCorreto = random.nextInt(3);
        RadioButton rbCorreto = radioButtonList.get(radioButtonCorreto);
        rbCorreto.setText(resposta.toString());
        int valorAnterior=-1;
        for(int i = 0; i<radioButtonList.size();i++){
            if(radioButtonList.get(i)!= rbCorreto){
                int valor = random.nextInt(10);
                if (valor == resposta || valor == valorAnterior) {
                    while (valor == resposta || valor == valorAnterior)
                        valor = random.nextInt(10);
                }
                radioButtonList.get(i).setText(String.valueOf(valor));
                radioButtonList.get(i).setTag(valor);
                valorAnterior = valor;
            }
        }

    }
}
