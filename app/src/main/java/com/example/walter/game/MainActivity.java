package com.example.walter.game;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button bContagem;
    Button bAritmetica;
    Button bMaiorNumero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bContagem = findViewById(R.id.botao_contagem);
        bAritmetica = findViewById(R.id.botao_aritmetica);
        bMaiorNumero = findViewById(R.id.botao_maior_numero);

    }
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.botao_contagem:
                        Intent itContagem = new Intent(MainActivity.this, ContagemActivity.class);
                        startActivity(itContagem);
                        break;
                    case R.id.botao_aritmetica:
                        Intent itAritmetica = new Intent(MainActivity.this, AritmeticaBasicaActivity.class);
                        startActivity(itAritmetica);
                        break;
                    case R.id.botao_maior_numero:
                        Intent itMaiorNumero = new Intent(MainActivity.this, MaiorNumeroActivity.class);
                        startActivity(itMaiorNumero);
                        break;
                }
            }


}
