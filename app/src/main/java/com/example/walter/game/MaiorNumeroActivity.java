package com.example.walter.game;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MaiorNumeroActivity extends AppCompatActivity {
    TextView num1;
    TextView num2;
    TextView num3;
    int num;
    static int r1, r2, r3;
    static int resp;
    static int acertou = 0;
    static int nota = 0;
    static int vez = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maior_numero);

        //Função random
        setNumero();
    }

    public void onClick(View view){
        EditText input = findViewById(R.id.resposta);

        AlertDialog.Builder builder = new AlertDialog.Builder(MaiorNumeroActivity.this);
        builder.setTitle("Resultado");
        String message = "";

        if(input.length()==0){
            message = "Insira um número.";
        } else {

            int inputNumber = Integer.parseInt(input.getText().toString());

            resp = maiorNumero();

            if (resp == inputNumber) {
                message ="Acertou! A resposta está correta!";
                acertou++;
            } else {
                message = "Errou! A resposta é " + resp + "!";
            }

            if (vez < 5) {
                vez++;
                setNumero();
            } else {
                nota = (acertou * 100) / 5;
                message = "Resultado Final: " + acertou + " acertos." + "\n\nSua nota foi: " + nota;
                vez = 1;
                acertou = 0;
            }
        }

        builder.setMessage(message);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }
    public int numero(){
        Random r = new Random();
        num = r.nextInt(10);
        return num;
    }

    //exibe na tela os 3 número aleatórios
    public void setNumero(){

        num1 = findViewById(R.id.num1);
        num2 = findViewById(R.id.num2);
        num3 = findViewById(R.id.num3);

        r1 = numero();
        r2 = numero();
        r3 = numero();

        //passa pra view

        num1.setText(String.valueOf(r1));
        num2.setText(String.valueOf(r2));
        num3.setText(String.valueOf(r3));

    }

    //pega os números separados e transforma em um número inteiro;
    public int maiorNumero(){

        //ve quem é o maior número para fazer a combinação
        if ((r1 >= r2) && (r1 >= r3) && (r2 >= r3)){
            return combinacao(r1, r2, r3);
        }
        if ((r1 >= r2) && (r1 >= r3) && (r3 >= r2)){
            return combinacao(r1, r3 , r2);
        }
        if ((r2 >= r1) && (r2 >= r3) && (r1 >= r3)){
            return combinacao(r2, r1, r3);
        }
        if ((r2 >= r1) && (r2 >= r3) && (r3 >= r2)){
            return combinacao(r2, r3, r1);
        }
        if ((r3 >= r1) && (r3 >= r2) && (r1 >= r2)){
            return combinacao(r3, r1, r2);
        }
        else {
            return combinacao(r3, r2, r1);
        }
    }
    //método que faz a combinação
    public int combinacao(int centena, int dezena, int unidade){

        int numero;
        int cent = centena * 100;
        int dez = dezena * 10;
        int uni = unidade;

        //retorna todos os números em um inteiro
        return numero = cent + dez + uni;
    }

}