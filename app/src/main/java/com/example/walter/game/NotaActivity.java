package com.example.walter.game;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NotaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        String nota = bundle.getString("nota");

        TextView txtResultado = (TextView) findViewById(R.id.resultado);
        txtResultado.setText(nota);

    }

    public void onClick(View view) {
        Intent itContagem = new Intent(NotaActivity.this, MainActivity.class);
        startActivity(itContagem);
    }
}
